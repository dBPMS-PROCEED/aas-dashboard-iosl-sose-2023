# AAS Dashboard - IoSL SoSe 2023

The AAS Dashboard is a web application that provides a clear visualization of an AAS (Asset Administration Shell). It allows users to manage and interact with multiple AAS files, modify existing ones, verify them against the specification, and create new AAS files and submodels. Additionally, the dashboard includes various charts to present relevant data in a visually appealing manner.

## Installation

To get started with the AAS Dashboard, follow the steps below:

1. Clone the repository and install NPM extensions(make sure that [GIT](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) and [NPM](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) are installed):

   ```
   git clone git@gitlab.com:dBPMS-PROCEED/aas-dashboard-iosl-sose-2023.git
   cd aas-dashboard-iosl-sose-2023\dashboard\
   npm install
   ```

2. Ensure Docker is installed:
   The AAS Dashboard is containerized using Docker, so make sure you have Docker installed on your machine. You can download Docker from the official website: [Docker Download](https://www.docker.com/products/docker-desktop)

3. Build the Docker image:
   Open a terminal, navigate to the root directory of the cloned repository, and run the following command to build the Docker image:

   ```
   cd ..
   docker build -t dashboard .
   ```

4. Run the Docker container:
   After building the Docker image, use the following command to run the AAS Dashboard container:

   ```
   docker run -d -p 3000:3000 dashboard
   ```

   The `-d` flag runs the container in detached mode, and the `-p 3000:3000` flag maps port 3000 from the container to port 3000 on your localhost, enabling you to access the app at [http://localhost:3000/](http://localhost:3000/).
   Wait few seconds before accessing the app via your browser.

5. Trying the app:

    There are a lot of avaible files in the folder `dashboard/public/machines` so that you can open them and test them.

5. Trying the app:
   
   Wait few seconds before accessing the app via your browser.
   
    There are a lot of avaible files in the folder `dashboard/public/machines` so that you can open them and test them.

## Features

The AAS Dashboard offers the following key features:

1. Clear Visualization:
   The dashboard provides a user-friendly and visually appealing interface to display AAS data and related information.

2. Multiple AAS Management:
   Easily manage and work with multiple AAS files within the dashboard.

3. AAS Modification:
   Edit and modify existing AAS files directly from the dashboard.

4. AAS Verification:
   Verify AAS files against the AAS specification to ensure compliance and correctness.

5. Create New AAS Files and Submodels:
   Create new AAS files and submodels effortlessly to add or expand your asset administration.

6. Charts:
   Access various charts to gain insights and analyze AAS data in a graphical format.

## Accessing the AAS Dashboard

Once the Docker container is up and running, you can access the AAS Dashboard by opening your web browser and navigating to [http://localhost:3000/](http://localhost:3000/).

Start exploring the AAS data, visualizations, and take advantage of the dashboard's powerful features to manage and interact with your Asset Administration Shells efficiently.
