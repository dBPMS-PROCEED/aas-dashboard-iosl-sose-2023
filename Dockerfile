# Use an official Node.js runtime as the base image
FROM node:14

# Set the working directory inside the container
WORKDIR /app

# Copy the package.json and package-lock.json to the container
COPY dashboard/package*.json ./

# Copy the rest of the app's files to the container
COPY dashboard/ .

# Install app dependencies
RUN npm run build

# Expose the port the app is listening on
EXPOSE 3000

# Start the app when the container runs
CMD ["npm", "start"]
