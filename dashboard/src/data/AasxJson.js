import JSZip from 'jszip';

export async function parseAasxJson(fileContent) {
  const zip = new JSZip();
  await zip.loadAsync(fileContent);

  for (let zipEntry of Object.values(zip.files)) {
    if (zipEntry.name.endsWith('.json')) {
      const jsonContent = await zipEntry.async('text');
      return JSON.parse(jsonContent);
    }
  }

  console.error("No .json file found in the AASX package");
  return null;
}
