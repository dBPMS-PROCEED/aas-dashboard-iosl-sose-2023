import React, { useState } from 'react';
import { Button, Modal } from 'antd';
import JSZip from 'jszip';
import { useAasFileStore } from './AasRegistry';
import placeholderImage from "../../pictures/aasPlaceholder.png";

function FileDownload({ initialFormat, displayName, treeData }) {
  const [downloadModalVisible, setDownloadModalVisible] = useState(false);
  const [downloadFormat, setDownloadFormat] = useState(initialFormat);
  const { aasFileDownload } = useAasFileStore();

  const handleDownloadClick = () => {
    setDownloadModalVisible(true);
  };

  const handleDownloadModalOk = () => {
    setDownloadModalVisible(false);
    handleDownload(transformTreeDataToJson(treeData), downloadFormat, displayName);
  };

  const handleDownloadModalCancel = () => {
    setDownloadModalVisible(false);
  };

  const transformTreeDataToJson = (treeData) => {
    return treeData.map((node) => {
      const { title, children, leaves } = node;

      if (children && children.length > 0) {
        const transformedChildren = transformTreeDataToJson(children);
        return {
          [title]: transformedChildren.length === 1 ? transformedChildren[0] : transformedChildren,
        };
      } else {
        return {
          [title]: leaves,
        };
      }
    });
  };

  const handleDownload = (data, format, displayName) => {
    let fileName = displayName? displayName: "aasDashboardFile";
    let content;
    let mimeType;

    if (format === 'json') {
      fileName += '.json';
      content = aasFileDownload.content;
      mimeType = 'application/json';
    } else if (format === 'aasx') {
      handleDownloadAASX(aasFileDownload.content, fileName, format);
      return;
    } else {
      console.error('Unsupported format:', format);
      return;
    }

    const element = document.createElement('a');
    const file = new Blob([content], { type: mimeType });
    element.href = URL.createObjectURL(file);
    element.download = fileName;
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  };

  const handleDownloadAASX = (content, fileName, format) => {
    const zip = new JSZip();
    const picturePath = `picture.png`;
    const pictureContent = aasFileDownload.image || placeholderImage;
    const base64Content = pictureContent.includes(",") ? pictureContent.split(",")[1] : pictureContent;
    zip.file(picturePath, base64Content, { base64: true });

    const mimeType = 'application/json';
    const file = new Blob([content], { type: mimeType });
    const aasxFolder = zip.folder(`aasx`);
    const filePath = `NameOfTheContentFile.json`;
    aasxFolder.file(filePath, file);

    zip.generateAsync({ type: 'blob' }).then(blob => {
      const url = URL.createObjectURL(blob);
      const link = document.createElement("a");
      link.href = url;
      link.download = `${fileName}.aasx`;
      link.click();
    });
  };

  return (
    <React.Fragment>
      <Button className='download-btn' onClick={handleDownloadClick}>
        Download
      </Button>

      <Modal
        title="Choose Download Format"
        visible={downloadModalVisible}
        onOk={handleDownloadModalOk}
        onCancel={handleDownloadModalCancel}
      >
        <p>Select the format for download:</p>
        <Button
          type={downloadFormat === 'json' ? 'primary' : 'default'}
          onClick={() => setDownloadFormat('json')}
          style={{ marginRight: '8px' }}
        >
          JSON
        </Button>
        <Button
          type={downloadFormat === 'aasx' ? 'primary' : 'default'}
          onClick={() => setDownloadFormat('aasx')}
        >
          AASX
        </Button>
      </Modal>
    </React.Fragment>
  );
}

export default FileDownload;
