import JSZip from 'jszip';

/**
 * Extracts the content of an AASX (Asset Administration Shell Exchange) file.
 * 
 * @param {File} file - The AASX file to extract content from.
 * @returns {Object} An object containing the file content, the file type, and the file image if it exists.
 */
export async function extractAasxContent(file) {
  // Determine the file type
  const fileType = file.name.split('.').pop().toLowerCase() === 'aasx' 
    ? 'application/zip' 
    : file.type;

  console.log('File type:', fileType);

  let fileContent = null;
  let zipFileType = null;
  let fileImage = null;

  // Process the file if it's a ZIP file
  if (fileType === 'application/zip' || fileType === 'application/x-zip-compressed') {
    try {
      const zip = new JSZip();
      await zip.loadAsync(file);

      // Look for an image in the ZIP file
      for (let zipEntry of Object.values(zip.files)) {
        if (zipEntry.name.endsWith('.png')) {
          fileImage = await zipEntry.async('base64');
          console.log("Found an image");
        }
      }

      // Filter the files in the ZIP archive to only include .json and .xml files in the 'aasx' folder
      const files = zip.filter((relativePath, file) => 
        relativePath.startsWith('aasx/') && 
        (relativePath.endsWith('.json') || relativePath.endsWith('.xml'))
      );

      console.log('Filtered files:', files); // Log the filtered files

      // Process the first file that matches the condition
      if (files.length > 0) {
        const zipEntry = files[0];
        fileContent = await zipEntry.async('text');
        zipFileType = zipEntry.name.endsWith('.json') ? 'application/json' : 'text/xml';
      }

      console.log('Parsed data:', fileContent);
    } catch (error) {
      console.error('Error parsing .aasx file:', error);
    }
  }

  return {
    fileContent,
    zipFileType,
    fileImage,
  };
}
