import React from "react";
import { Table } from 'antd';

function TableView({ files, handleContentClick, handleExpand, expandedRowKeys }) {
  const columns = [
    {
      title: 'Name',
      dataIndex: 'displayName',
      key: 'displayName',
    },
    {
      title: 'Asset Kind',
      dataIndex: 'assetKind',
      key: 'asset kind',
    },
    {
      title: 'Number of Subelements',
      dataIndex: 'numberOfSubmodelElements',
      key: 'Number of Subelements',
    },
    {
      title: 'Last Modified',
      dataIndex: 'lastModified',
      key: 'lastModified',
    },
  ];
  const submodelColumns = [
    {
      dataIndex: 'idShort',
      key: 'idShort',
    }
    // Add more columns as needed based on the properties of your submodels
  ];

  return (
    <div style={{marginTop: '20px'}}>
      {files.length > 0 && (
        <Table  columns={columns} dataSource={files} pagination={false} expandedRowKeys={expandedRowKeys}
             onExpand={handleExpand} expandable={{
          expandedRowRender: (record) => (
      <Table
      columns={submodelColumns}
      dataSource={record.submodels}
      pagination={false}
      expandable={{
        expandedRowRender: (record2) => (
          <Table
            columns={submodelColumns}
            dataSource={record2.submodelElements}
            pagination={false}
          />
        ),
      }}
    />
  ),
}}
onRow={(record) => {
  return {
    onClick: () => handleContentClick(record), // click row
    style: { cursor: 'pointer' }, // Set cursor to pointer
  };
}}
/>
)}
    </div>
  );
}

export default TableView;
