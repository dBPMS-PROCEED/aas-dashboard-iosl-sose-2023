import { AasFile } from "./AasFile";
import create from 'zustand';

/**
 * Class to manage a registry of AAS (Asset Administration Shell) files.
 */
export class AasRegistry {
  constructor() {
    this.aasFiles = [];
  }

  /**
   * Adds multiple files to the registry.
   * 
   * @param {Array} files - The files to be added to the registry.
   */
  addFiles(files) {
    files.forEach(file => {
      const aasFile = new AasFile(file.content, file.type, file.lastModified, file.image, file.name);

      // Determine the key for the AAS file
      if (aasFile.overview) {
        aasFile.key = aasFile.overview.id;
      } else if (aasFile.displayName) {
        aasFile.key = aasFile.displayName;
      } else {
        aasFile.key = "tempKey";
      }

      this.aasFiles.push(aasFile);
    });
  }

  /**
   * Returns the files in the registry.
   * 
   * @returns {Array} The files in the registry.
   */
  getFiles() {
    return this.aasFiles;
  }

  /**
   * Returns the content of a file by its name.
   * 
   * @param {string} name - The name of the file.
   * @returns {Object} The file content, or null if the file is not found.
   */
  getContentByName(name) {
    const file = this.aasFiles.find(aasFile => aasFile.fileName === name);
    return file ? file.content : null;
  }

  // You can add methods here to manipulate or retrieve data from the AAS files
}

/**
 * Zustand store for the AAS registry.
 */
export const useAasRegistryStore = create(set => ({
  aasRegistry: new AasRegistry(),
  setAasRegistry: (aasRegistry) => set({ aasRegistry }),
}));

/**
 * Zustand store for the AAS file download.
 */
export const useAasFileStore = create(set => ({
  aasFileDownload: null,
  setAasFileDownload: (aasFileDownload) => set({ aasFileDownload }),
}));
