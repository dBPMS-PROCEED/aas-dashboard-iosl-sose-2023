import React, { useState } from 'react';
import { useLocation } from 'react-router-dom';
import { HomeOutlined } from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';
import { Row, Button , Col} from 'antd';
import SubmodelTree from './SubmodelTree';
import placeholderImage from "../../pictures/aasPlaceholder.png";
import FileDownload from './FileDownload'; // Import the DownloadComponent
import MyTree from './MyTree';

function MachinePage() {
  console.log("main page");
  const location = useLocation();
  const [treeData, setTreeData] = useState(location.state.treeData);
  const image = location.state.image;
  const aasImage = image ? `data:image/png;base64,${image}` : placeholderImage;
  const navigate = useNavigate();
  const [aasFile, setAasFile] = useState(location.state.aasFile);

  const updateAasFile = (updatedFile) => {
    setAasFile(updatedFile);
  };

  const handleReturnClick = () => {
    navigate(-1); // Navigates to the previous page
  };


  let displayName = treeData[0]?.children[0]?.children.find(
    (child) => child.title === 'displayName'
  )?.leaves?.text;

  return (
    <div>
      <h1 className='machine-page-display-name'>{displayName}</h1>
      <Row>
        <Button
          className='home-btn'
          onClick={handleReturnClick}
          icon={<HomeOutlined />}
        />
        <div style={{ marginLeft: '2.5%' }} />
        <SubmodelTree aasFile={aasFile} treeData={treeData} setTreeData={setTreeData} />

        <FileDownload 
          initialFormat="json" 
          displayName={displayName} 
          treeData={treeData}
        /> 
      </Row>

      <div className='machine-page-layout'>
        <Col md={{ span: 7, offset: 1 }} lg={{ span: 5, offset: 1 }}>
          <img
            src={aasImage}
            alt="pic"
            className='machinePage-img'
          />
        </Col>
        <Col md={16} lg={14}>
        <div className='machinePage-div'>
        <MyTree treeData={treeData} image ={image} aasFile={aasFile} updateAasFile={updateAasFile} />
    </div>
        </Col>
      </div>
    </div>
  );
}

export default MachinePage;
