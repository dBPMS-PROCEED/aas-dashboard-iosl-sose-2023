import React from 'react';
import { Tabs , Col } from 'antd';
import { useNavigate } from 'react-router-dom';
import logo from "../../data/P.png";

const { TabPane } = Tabs;

function NavigationTabs() {
  const navigate = useNavigate();

  const items = [
    {
      key: '1',
      label: `Overview`,
      path: '/',
    },
    {
      key: '2',
      label: `Charts`,
      path : '/charts'
    },
  ];

  return (
    
    <header className='header'>
       <Col sm={8} md={6} lg={4} offset={1} >
         <img src={logo} alt="Logo"  className='logo-img'/> 
       </Col>   
       <Col  className= 'temp' md={1} lg={{span: 1, offset: 1}}></Col>
       <Col  sm={{span: 5, offset: 0}} md={{span: 5, offset: 2}} lg={{span: 3, offset: 3}} > 
        <Tabs defaultActiveKey="1" tabStyle={{ color: '#D85031', fontWeight: 'bold' }} className= 'Tabs' >
          {items.map(item => (
          <TabPane
          tab={
            <div
              onClick={() => navigate(item.path)} className= 'tabPane'>
                {item.label}
            </div>}
          key={item.key}
          /> ))}
       </Tabs>
       </Col> 
      <Col  className= 'temp2'></Col>
      <Col  sm={{span: 12, offset: 5}} md={{span: 8, offset: 4}} lg={{span: 3, offset: 7}}  > 
       <h1 className= 'proceed'>PROCEED </h1>
      </Col>

    </header>
    
  );
}

export default NavigationTabs;
