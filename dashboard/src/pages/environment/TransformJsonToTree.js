/**
 * Transforms a JSON object into a tree data structure.
 * 
 * @param {Object} json - The input JSON object.
 * @param {string} [path = "environment"] - The path to the current node in the tree.
 * @param {boolean} [isChildOfSubmodels = false] - Flag to check if the current node is a child of "submodels".
 * @returns {Array} The tree data structure.
 */
export function transformJsonToTreeData(json, path = "environment", isChildOfSubmodels = false) {
  // Check if the input is an array.
  if (Array.isArray(json)) {
    return json.map((item, index) => {
      const newPath = `${path}[${index}]`;
      return transformJsonToTreeData(item, newPath, isChildOfSubmodels)[0];
    });

  // Check if the input is a non-null object.
  } else if (typeof json === "object" && json !== null) {
    const children = [];
    const leaves = {};

    // Iterate over each key-value pair in the JSON object.
    Object.entries(json).forEach(([key, value]) => {
      const newPath = path ? `${path}.${key}` : key;

      // Special handling for "submodels" and "conceptDescriptions".
      if (key === "submodels" || key === "conceptDescriptions") {
        const valueArray = Array.isArray(value) ? value : [value];
        const childNodes = transformJsonToTreeData(valueArray, newPath, key === "submodels");
        children.push({
          title: key,
          key: newPath,
          children: childNodes,
        });

      // Check if the value is a non-null object.
      } else if (typeof value === "object" && value !== null) {
        const childData = transformJsonToTreeData(value, newPath, isChildOfSubmodels);
        if (childData.length > 0) {
          children.push(...childData);
        }

      // If the value is not an object, treat it as a leaf node.
      } else {
        leaves[key] = value;
      }
    });

    // Build the tree node.
    const lastSegmentOfPath = path.split(".").pop().replace(/\[\d+\]$/, "");
    const title = lastSegmentOfPath === "assetAdministrationShells" 
      ? "assetAdministrationShells" 
      : (leaves.idShort || lastSegmentOfPath);

    return [{
      title,
      key: path,
      children,
      leaves
    }];

  // If the input is not an array or a non-null object, return an empty array.
  } else {
    return [];
  }
}
