import React, { useState, useEffect, useRef } from "react";
import { Button, Input, Tree, Modal, Select } from "antd";
import { SubmodelTemplates } from "./SubmodelTemplates";

const { TreeNode } = Tree;
const { Option } = Select;

const SubmodelTree = ({
  aasFile,
  setTreeData,
  handleInputChange,
}) => {
  const [templates, setTemplates] = useState(null);
  const selectedValueRef = useRef(null);

  const modalRef = useRef(); // Create a new reference for the modal instance

  useEffect(() => {
    console.log("Running useEffect");
    const submodelTemplates = new SubmodelTemplates();

    setTemplates(submodelTemplates?.templates);

    return () => {
      console.log("Cleaning up modal");
      if (modalRef.current) {
        modalRef.current.destroy();
        modalRef.current = null; // Set it to null after destroying
      }
    };
  }, []);


  const handleSelection = (value) => {
    console.log("Selection changed:", value);
    selectedValueRef.current = value;
  };

  const handleOk = () => {
    console.log("Modal OK clicked");
    console.log("Selected item:", selectedValueRef.current);

    // Find the new node in the templates using the selected value.
    const newNode = templates.find(
      (element) => element.submodelId === selectedValueRef.current
    );
    
    // Check if the new node and its content exist.
    if (
      newNode &&
      newNode.subModelElements &&
      newNode.subModelElements.content
    ) {
      const newNodeContent = newNode.subModelElements.content;

      // If the new node content exists, create new submodels from it.
      if (newNode.subModelElements.content) {
        const newSubmodels = newNodeContent.children.map(
          (child) => addSubmodelToAas(child)
        );

        // Create submodel objects from the new submodels.
        const submodelObjects = newSubmodels.map((newSubmodel, index) => {
          return {
            id: newNodeContent.leaves.id,
            modelType: "Submodel",
            submodelElements: newSubmodel.submodels,
          };
        });

        // Parse the content of the AAS file.
        let aasContent = JSON.parse(aasFile.content);

        // If submodels don't exist in the content, initialize it as an empty array.
        if (!aasContent.submodels) {
          aasContent.submodels = [];
        }

        // Push each submodel object to the submodels array in the content.
        submodelObjects.forEach((submodelObject) =>
          aasContent.submodels.push(submodelObject)
        );

        // Update the AAS file content by stringifying the updated content.
        aasFile.content = JSON.stringify(aasContent);
      } else {
        console.error("newNode.subModelElements.content is undefined");
      }
    }

    // If concept descriptions exist in the new node, create new concept descriptions from it.
    if (newNode.conceptDescriptions) {
      const newConceptDescriptions = newNode.conceptDescriptions.map(
        (description) => addConceptDescriptionsToAas(description)
      );
    
      // Create concept description objects from the new concept descriptions.
      const conceptDescriptionObjects = newConceptDescriptions.map((newDescription, index) => {
        return {
          id: newNode.conceptDescriptions[index].leaves.id,
          modelType: "ConceptDescription",
          conceptDescriptions: newDescription.conceptDescriptions,
        };
      });
    
      // Parse the content of the AAS file.
      let aasContent = JSON.parse(aasFile.content);

      // If concept descriptions don't exist in the content, initialize it as an empty array.
      if (!aasContent.conceptDescriptions) {
        aasContent.conceptDescriptions = [];
      }

      // Push each concept description object to the concept descriptions array in the content.
      conceptDescriptionObjects.forEach((descriptionObject) =>
        aasContent.conceptDescriptions.push(descriptionObject)
      );

      // Update the AAS file content by stringifying the updated content.
      aasFile.content = JSON.stringify(aasContent);
      console.log("content", aasFile.content);
    
    } else {
      console.error("newNode.conceptDescriptions is undefined");
    }
    
    // Update the tree data.
    setTreeData((treeData) => {
      let updatedTreeData = [...treeData];
      
      // Update the environment tree of the AAS file.
      aasFile.updateEnvironmenTree();
      
      // Set the updated tree data as the environment tree of the AAS file.
      updatedTreeData = aasFile.environmentTree;
      console.log(aasFile);

      return updatedTreeData;
     });
};


  const addPropertyIfDefined = (submodel, returnObj, property) => {
    if (submodel.leaves?.[property]) {
      returnObj[property] = submodel.leaves[property];
    } else if (submodel.children) {
      const child = submodel.children.find(child => child.title === property);
      if (child) {
        returnObj[property] = handleComplexProperty(child);
        console.log(returnObj)
      }
    }
  };

  const handleComplexProperty = (complexProperty) => {
    //const key = complexProperty.
    if (complexProperty.children) {
        return {
          keys: [{
            value: "No value entered",
            type: "Submodel",
          }],
          type: "ExternalReference",
        };
      }
    return "No value entered";
  };
  
  const processSubmodelElementCollection = (submodel) => {
    return submodel.children.map(child => {
      let result = {};
      ['value', 'valueType', 'entityType', 'category', 'kind', 'direction', 'observed', 'state', 'contentType', 'typeValueListElement', 'first', 'second'].forEach(property => {
        addPropertyIfDefined(child, result, property);
      });
      return result;
    });
  };
  
  const processSemanticIdChild = (semanticIdChild) => {
    return semanticIdChild?.leaves ? {
      keys: [
        {
          value: semanticIdChild.leaves._ || "No value entered",
          type: semanticIdChild.leaves.type || "xs:string",
        },
      ],
      type: "ExternalReference",
    } : null;
  };
  
// This function is used to add a Submodel to the Asset Administration Shell (AAS). 
const addSubmodelToAas = (submodels, isTemplate = false) => {
  // Check if submodels is an array. If not, make it an array for ease of processing.
  const submodelsArray = Array.isArray(submodels) ? submodels : [submodels];
  
  // Check if the first submodel is of kind "Template" or if it is a template. If yes, process accordingly.
  if (submodelsArray.length > 0 && submodelsArray[0]?.leaves?.kind === "Template" || isTemplate) {
    return {
      // For each submodel in the array, process it and return the processed submodels.
      submodels: submodelsArray.map((submodel) => {
        // Process the semanticId child of the submodel.
        const semanticIdChild = processSemanticIdChild(submodel.children?.find((child) => child.title === "semanticId"));
        
        // Initialize the submodel properties with idShort and modelType.
        let submodelProperties = {
          idShort: submodel.leaves?.idShort || "No value entered",
          modelType: submodel.leaves?.modelType
            ? submodel.leaves.modelType.charAt(0).toUpperCase() +
              submodel.leaves.modelType.slice(1)
            : "No value entered",
        };
      
        // Add other properties to the submodel if they are defined.
        ['valueType', 'category', 'direction', 'state', 'observed', 'contentType', 'entityType', 'typeValueListElement', 'first', 'second'].forEach(property => {
          addPropertyIfDefined(submodel, submodelProperties, property);
        });

        // If the submodel is of type "submodelElementCollection", process it accordingly.
        if (submodel.leaves.modelType === "submodelElementCollection") {
          submodelProperties.value = processSubmodelElementCollection(submodel);
        }
        else {
          // Else, add the value property to the submodel.
          submodelProperties.value = {
            keys: 
              {
                value: submodel.leaves?.value?.keys || "No value entered",
                type: "GlobalReference",
              },
            type: "ExternalReference",
          };
        }

        // Add the semanticId child to the submodel.
        submodelProperties.semanticId = semanticIdChild;
        
        return submodelProperties;
      }),
    };
  }
  
  return {
    // If the submodel is not a template, process it differently.
    submodels: submodelsArray.map((submodel) => {
      let returnObj = {
        idShort: submodel.leaves?.idShort,
        modelType: submodel.leaves?.modelType
          ? submodel.leaves.modelType.charAt(0).toUpperCase() +
            submodel.leaves.modelType.slice(1)
          : "No value entered",
      };

      // Add properties to the submodel if they are defined.
      ['value', 'valueType', 'entityType', 'category', 'kind', 'direction', 'observed', 'state', 'contentType', 'typeValueListElement', 'first', 'second'].forEach(property => {
        addPropertyIfDefined(submodel, returnObj, property);
      });

      return returnObj;
    }),
  };
};

  
  


  const addConceptDescriptionsToAas = (conceptDescriptions) => {
    // If conceptDescriptions is not an array, put it into one
    const conceptDescriptionsArray = Array.isArray(conceptDescriptions) ? conceptDescriptions : [conceptDescriptions];
    return {
      conceptDescriptions: conceptDescriptionsArray.map((description) => {
        return {
          id: description.children[0].leaves._,
          idShort: description.leaves.idShort,
          modelType: "ConceptDescription" // assuming children should be kept as is
        };
      }),
    };
  };
  


  const addSubmodel = () => {
    console.log("Adding a submodel");
    if (!templates) {
      console.log("Templates not loaded yet");
      return;
    }

    const items = templates.map((template) => template.submodelId);

    modalRef.current = Modal.confirm({
      title: "Confirm",
      content: (
        <Select
          placeholder="Select an item"
          style={{ width: "100%" }}
          onChange={handleSelection}
        >
          {items.map((item) => (
            <Option value={item} key={item}>
              {item}
            </Option>
          ))}
        </Select>
      ),
      onOk: handleOk,
      onCancel: () => {
        console.log("Modal cancelled");
      },
    });
  };

  const renderTreeNodes = (data) =>
    data.map((node) => {
      const title = (
        <div>
          {node.title}:
          {node.children ? (
            <span>{node.value}</span>
          ) : (
            <Input
              value={node.value}
              onChange={(e) => handleInputChange(e, node.key)}
            />
          )}
        </div>
      );

      return (
        <TreeNode title={title} key={node.key} dataRef={node}>
          {node.children && renderTreeNodes(node.children)}
        </TreeNode>
      );
    });

  return (
    <div>
      <Button
        style={{
          marginLeft: "10%",
          marginRight: "40px",
          color: "white", // Change the font color to white
          backgroundColor: "#FF6600",
        }}
        onClick={addSubmodel}
      >
        Add Submodel
      </Button>
    </div>
  );
};

export default SubmodelTree;

const typeEnumDefXsd = [
  "xs:anyURI",
  "xs:base64Binary",
  "xs:boolean",
  "xs:byte",
  "xs:date",
  "xs:dateTime",
  "xs:decimal",
  "xs:double",
  "xs:duration",
  "xs:float",
  "xs:gDay",
  "xs:gMonth",
  "xs:gMonthDay",
  "xs:gYear",
  "xs:gYearMonth",
  "xs:hexBinary",
  "xs:int",
  "xs:integer",
  "xs:long",
  "xs:negativeInteger",
  "xs:nonNegativeInteger",
  "xs:nonPositiveInteger",
  "xs:positiveInteger",
  "xs:short",
  "xs:string",
  "xs:time",
  "xs:unsignedByte",
  "xs:unsignedInt",
  "xs:unsignedLong",
  "xs:unsignedShort",
];

const typeEnumIec61360 = [
  "BLOB",
  "BOOLEAN",
  "DATE",
  "FILE",
  "HTML",
  "INTEGER_COUNT",
  "INTEGER_CURRENCY",
  "INTEGER_MEASURE",
  "IRDI",
  "IRI",
  "RATIONAL",
  "RATIONAL_MEASURE",
  "REAL_COUNT",
  "REAL_CURRENCY",
  "REAL_MEASURE",
  "STRING",
  "STRING_TRANSLATABLE",
  "TIME",
  "TIMESTAMP",
];

const getEnumValue = (input, type) => {
  let enums;
  if (type === "DataTypeDefXsd") {
    enums = typeEnumDefXsd;
  } else if (type === "DataTypeIec61360") {
    enums = typeEnumIec61360;
  } else {
    return "Invalid type";
  }

  const result = enums.find((enumValue) =>
    enumValue.toLowerCase().endsWith(input.toLowerCase())
  );
  return result || "Enum not found";
};


