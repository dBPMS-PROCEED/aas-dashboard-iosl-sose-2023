import React from 'react';
import schemaFile from "./schema.json";
import { extractAasxContent } from './extractAasxContent';
import Ajv from "ajv/dist/2019";
import xsdSchema from "./schema.xsd";
import tv4 from 'tv4';

/**
 * Async function to validate XML content against a schema.
 *
 * @param {string} xmlContent - The XML content to validate.
 * @returns {boolean} - The validation result.
 */
async function checkxml(xmlContent) {
  const result = tv4.validateMultiple(xmlContent, xsdSchema);

  if (!result.valid) {
    console.error('XML validation error:', result.errors);
    window.alert('The XML file is not compatible with the specification.');
  }

  // Return the validation result.
  return result.valid;
}

const FileUploader = ({ onFileUpload }) => {
  /**
   * Async function to handle file upload.
   * It reads the files, extracts and validates the content, and then calls the onFileUpload callback.
   *
   * @param {Event} event - The file upload event.
   */
  const handleFileUpload = async (event) => {
    const files = event.target.files;
    const newUploadedFiles = [];

    // Process each uploaded file
    for (let i = 0; i < files.length; i++) {
      const file = files[i];

      // Extract the file content, type, and image (if any)
      const { fileContent, zipFileType, fileImage } = await extractAasxContent(file);

      const reader = new FileReader();
      reader.onload = async (e) => {
        // Get the content and type of the file
        const content = fileContent ? fileContent : e.target.result;
        const type = zipFileType ? zipFileType : file.type;

        // Validate the file content based on its type
        if (type === 'application/json') {
          const fileToCheck = JSON.parse(content);
          const ajv = new Ajv();
          const validate = ajv.compile(schemaFile);
          const isValid = validate(fileToCheck);

          if (!isValid) {
            window.alert("The file " + file.name + " is not compatible with the specification");
            return;
          }
        } else if (type === 'text/xml') {
          const isValid = await checkxml(content, schemaFile);
          if (!isValid) return;
        }

        // Add the file details to the list of uploaded files
        newUploadedFiles.push({
          content,
          name: file.name,
          type,
          lastModified: new Date(file.lastModified).toLocaleString(undefined, {
            year: 'numeric',
            month: 'long',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric',
          }),
          image: fileImage || null,
        });

        // If this is the last file, call the onFileUpload callback
        if (i === files.length - 1) {
          onFileUpload(newUploadedFiles);
        }
      };

      reader.onerror = (error) => {
        console.error('Error reading file:', error);
      };

      // Start reading the file as text
      reader.readAsText(file);
    }
  };

  return (
    <div className='fileUploader-btn'>
      <label htmlFor="file-upload" style={{ color: 'white'}}>
        Upload files
      </label>
      <input  
        id="file-upload" 
        type="file" 
        multiple 
        onChange={handleFileUpload}
        className='fileUploader-input'
      />
    </div>
  );
};

export default FileUploader;
