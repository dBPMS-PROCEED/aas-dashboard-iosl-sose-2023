import React from "react";
import { Col, Button, Row, Modal } from "antd";
import { HomeOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import emptyJson from "./emptyJson.json";
import SubmodelTree from "./SubmodelTree";
import { transformJsonToTreeData } from "./TransformJsonToTree";
import FileDownload from "./FileDownload";
import MyTree from "./MyTree";
import { useAasFileStore } from "./AasRegistry";
import { AasFile } from "./AasFile";

const NewFile = () => {
  const [treeData, setTreeData] = React.useState([]);
  const [downloadModalVisible, setDownloadModalVisible] = useState(false);
  const [downloadFormat, setDownloadFormat] = useState('json');
  const { aasFileDownload, setAasFileDownload } = useAasFileStore();

  const [aasFileTemp, setAasFileTemp] = React.useState([]);

  const handleDownloadModalOk = () => {
    setDownloadModalVisible(false);
  };
  
  const handleDownloadModalCancel = () => {
    setDownloadModalVisible(false);
  };

  React.useEffect(() => {
    const transformedJson = transformJsonToTreeData(emptyJson);
    const emptyJsonString = JSON.stringify(emptyJson);
    const aasFile = new AasFile(emptyJsonString);
    setAasFileTemp(aasFile);
    setTreeData(transformedJson);
  }, []);

  const navigate = useNavigate();
  const handleReturnClick = () => {
    navigate(-1); // Navigates to the previous page
  };
  const [selectedImage, setSelectedImage] = useState(null);

  const handleFileChange = (e) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (event) => {
        const imageDataURL = event.target.result;
        setSelectedImage(imageDataURL);
        console.log(aasFileDownload,"Download filer");
        setAasFileDownload({...aasFileDownload,content: aasFileDownload.content,image:imageDataURL});
      };
      reader.readAsDataURL(file);
    }
  };


  return (
    <div>
      <h2 style={{ marginBottom: "0px", marginLeft: "42%", color: "#0d3b66" }}>
        
      </h2>
      <Row>
        <Button
          className='home-btn'
          onClick={handleReturnClick}
          icon={<HomeOutlined />}
        />
         <div style={{marginLeft: '2%'}} />
         <SubmodelTree aasFile={aasFileTemp} treeData={treeData} setTreeData={setTreeData} />
         <FileDownload 
          initialFormat="json" 
          treeData={treeData}
        /> 
      </Row>
      <Row >
         <Col md={{span:7 ,offset:1}} lg={{span:5 ,offset:1}}>
          <input
            type="file"
            id="fileInput"
            accept="image/*"
            onChange={handleFileChange}
            style={{ display: "none" }}
          />
          {selectedImage && (
            <div>
              <img
                src={selectedImage}
                alt="Selected"
                style={{ maxHeight: "250px", maxWidth: "250px" }}
              />
            </div>
          )}
          {!selectedImage && (
            <label
              htmlFor="fileInput"
              style={{
                border: "1px dashed #0d3b66",
                padding: "6px 12px",
                cursor: "pointer",
                
                color: "#0d3b66",
                textAlign: "center",
                
                marginLeft: "30px",
                marginTop: '900px',
              }}
            >
              Upload machine image
            </label>
          )}
        </Col>
        <Col  md={16} lg={14}>
          <MyTree 
            treeData={treeData} 
            aasFile={aasFileTemp} 
          />
        </Col>
      </Row>
      <Modal
        title="Choose Download Format"
        visible={downloadModalVisible}
        onOk={handleDownloadModalOk}
        onCancel={handleDownloadModalCancel}
      >
        <p>Select the format for download:</p>
        <Button
          type={downloadFormat === 'json' ? 'primary' : 'default'}
          onClick={() => setDownloadFormat('json')}
          style={{ marginRight: '8px' }}
        >
          JSON
        </Button>
        <Button
          type={downloadFormat === 'aasx' ? 'primary' : 'default'}
          onClick={() => setDownloadFormat('aasx')}
        >
          AASX
        </Button>
      </Modal>
      
      
    </div>
  );
};

export default NewFile;
