import { extractAasxContent } from "./extractAasxContent";
import { transformJsonToTreeData } from "./TransformJsonToTree";
import { v4 as uuidv4 } from "uuid";

function delay(time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

export class SubmodelTemplates {
  constructor() {
    this.templates = [];
    this.loadTemplates();
  }

  loadTemplates() {
    // Fetch the manifest file
    fetch("/templates/manifest.json")
      .then((response) => {
        if (!response.ok) {
          throw new Error("HTTP error " + response.status);
        }
        return response.json();
      })
      .then(async (manifest) => {
        // Load each template
        for (const file of manifest.files) {
          console.log("submodel File", file);
          const template = new SubmodelTemplate(file);

          let attempts = 0;
          while (template.submodelId === null && attempts < 10) {
            // Wait for 500ms
            await delay(500);
            console.log("waiting");
            attempts++;
          }

          this.templates.push({
            submodelId: template.submodelId,
            subModelElements: template.subModelElements,
            conceptDescriptions: template.conceptDescriptions,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }
}

class SubmodelTemplate {
  constructor(File) {
    this.submodelId = null;
    this.xmlFile = null;
    this.jsonFile = null;
    this.conceptDescriptions = null;
    this.subModelElements = {
      id: null,
      content: null,
    };
    this.loadTemplate(File);
  }

  async loadTemplate(file) {
    // Construct the URL of the aasx file
    const url = `/templates/${file}`;

    const isJsonFile = url.endsWith(".json");

    // If the file is a JSON, we can fetch and parse it directly
    if (isJsonFile) {
      fetch(url)
        .then((response) => response.json()) // Fetch and parse JSON directly
        .then((jsonContent) => {
          this.jsonFile = jsonContent;
          this.extractDataFromTemplateJson(this.jsonFile);
        })
        .catch((error) => {
          console.log("Error loading JSON file:", error);
        });
    } else {
      // Fetch the aasx file
      fetch(url)
        .then((response) => {
          if (!response.ok) {
            throw new Error("HTTP error " + response.status);
          }
          return response.blob();
        })
        .then((blob) => {
          // Change the extension of the file to .zip
          const zipFile = new File([blob], "template.zip", {
            type: "application/zip",
          });
          // Extract the content of the .zip file
          return extractAasxContent(zipFile);
        })
        .then(({ fileContent, zipFileType }) => {
          // Parse the XML file content
          if (zipFileType === "text/xml") {
            this.xmlFile = fileContent;
            const xml2js = require("xml2js");
            let jsonable = null;
            xml2js.parseString(
              this.xmlFile,
              { explicitArray: false, mergeAttrs: true },
              (err, result) => {
                if (err) {
                  console.error(err);
                } else {
                  jsonable = JSON.stringify(result, null, 2);
                }
              }
            );
            jsonable = JSON.parse(jsonable);
            this.removePrefix(jsonable);
            this.extractDataFromTemplate(jsonable);
          }
        })
        .catch((error) => {
          console.log("Error loading AASX file:", error);
        });
    }
  }

// This function is responsible for extracting data from a template JSON file.
extractDataFromTemplateJson(file) {
  // Get the submodel elements and the submodel identification ID from the file.
  const submodelElements = file.submodels[0].submodelElements[0];
  const submodelIdentifcationId = file.submodels[0].id;

  // Get the submodel ID from the submodel elements.
  const submodelId = submodelElements.modelType;

  // Transform the submodels to tree data.
  const processedSubmodelElements = this.transformToTreeData(file.submodels);
  
  // Update the submodel ID and submodel elements ID.
  this.submodelId = submodelId;
  this.subModelElements.id = submodelId;
  
  // Update the content of the submodel elements.
  this.subModelElements.content = {
    title: submodelElements.modelType,
    key: `${submodelId}emptyTemplate${uuidv4()}`,
    children: processedSubmodelElements[0].children,
    leaves: { id: submodelIdentifcationId },
  };
}


// This function is responsible for extracting data from a template.
extractDataFromTemplate(file) {
  // Get the submodels and concept descriptions from the file.
  const submodels = file.aasenv.submodels;
  const conceptDescriptions = file.aasenv.conceptDescriptions;
  
  // Transform the concept descriptions to tree data.
  this.conceptDescriptions = transformJsonToTreeData(conceptDescriptions.conceptDescription);

  // Get the short ID of the submodel.
  const submodelIdShort = submodels.submodel.idShort;

  // Get the submodel elements from the submodel.
  let subModelElements = submodels.submodel.submodelElements.submodelElement;

  // If submodel elements is not an array, get the elements from the submodel element collection.
  if (!Array.isArray(subModelElements)) {
    subModelElements = subModelElements.submodelElementCollection.value.submodelElement;
  }
  
  // Process the submodel elements and concept descriptions.
  const processedSubmodelElements = this.processSubmodelElements(subModelElements, conceptDescriptions);

  // Update the submodel ID.
  this.submodelId = `Template: ${submodelIdShort}`;
  
  // Update the submodel elements ID.
  this.subModelElements.id = processedSubmodelElements.map(element => element.idShort);
  
  // Update the content of the submodel elements.
  this.subModelElements.content = {
    title: this.submodelId,
    key: `${this.submodelId}${uuidv4()}`,
    children: processedSubmodelElements,
    leaves: {
      id: submodels.submodel.identification._,
      modelType: "Submodel",
    },
  };
}

  transformToTreeData(dataArray, path = "") {
    return dataArray
      .map((item, index) => {
        const newPath = `${path}[${index}]`;

        if (typeof item === "object" && item !== null) {
          const children = [];
          const leaves = {};

          Object.entries(item).forEach(([key, value]) => {
            const nextPath = `${newPath}.${key}`;

            if (typeof value === "object" && value !== null) {
              if (Array.isArray(value)) {
                children.push(...this.transformToTreeData(value, nextPath));
              } else {
                const [child] = this.transformToTreeData([value], nextPath);
                if (child.title === "") {
                  // Merge properties of child node with root if title is empty
                  children.push(...child.children);
                  Object.assign(leaves, child.leaves);
                } else {
                  children.push(child);
                }
              }
            } else {
              leaves[key] = value;
            }
          });

          return {
            title:
              leaves.idShort ||
              path
                .split(".")
                .pop()
                .replace(/\[\d+\]$/, ""),
            key: newPath,
            children,
            leaves,
          };
        }
      })
      .filter((node) => node); // This will remove any null items
  }

  processSubmodelElements(submodelElements) {
    // Map over submodelElements, transforming each element
    return submodelElements
      .map((element) => {
        // Determine the modelType of the element
        const modelType = Object.keys(element)[0];

        // Get the data corresponding to the modelType
        const modelData = element[modelType];

        let modelValue = null;
        const semanticId = modelData.semanticId.keys.key;

        // If the modelType is a submodelElementCollection, further process its elements
        if (modelType === "submodelElementCollection") {
          let elementsToProcess = [];

          if (modelData.value && modelData.value.submodelElement) {
            elementsToProcess = Array.isArray(modelData.value.submodelElement)
              ? modelData.value.submodelElement
              : [modelData.value.submodelElement];
          }
          // Recursively call processSubmodelElements on elementsToProcess
          modelValue = this.processSubmodelElements(elementsToProcess);
        }
        // Create a node for the semanticId
        const semanticIdNode = {
          title: "semanticId",
          key: "semanticId" + semanticId._,
          children: [],
          leaves: semanticId,
        };
        // Define the content for this node
        const content = {
          idShort: modelData.idShort ?? null,
          category: modelData.category ?? null,
          kind: modelData.kind ?? null,
          value: modelData.value ?? "No value entered",
          valueType: modelData.valueType ?? null,
          modelType: modelType ?? null,
          direction: modelData.direction ?? null,
          observed: modelData.observed ?? null,
          state: modelData.state ?? null,
          contentType: modelData.contentType ?? null,
          entityType: modelData.entityType ?? null,
          typeValueListElement: modelData.typeValueListElement ?? null,
          first: modelData.first ?? null,
          second: modelData.second ?? null,
        };
        // Generate a unique ID for the node if idShort does not exist
        const tempId4 = uuidv4();
        return {
          title: modelData.idShort ?? null,
          key: modelData.idShort ?? "tempSubmodelElement" + tempId4,
          children: [...(modelValue ?? []), semanticIdNode],
          leaves: content,
        };
      })
      .flat();
  }

  // Function to recursively remove 'aas:' prefix from keys
  removePrefix(obj) {
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        let newKey = key.replace("aas:", "");
        if (newKey !== key) {
          obj[newKey] = obj[key];
          delete obj[key];
        }
        if (typeof obj[newKey] === "object") {
          this.removePrefix(obj[newKey]);
        }
      }
    }
  }
}
