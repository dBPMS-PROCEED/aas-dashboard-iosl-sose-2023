import React, { useState } from 'react';
import { Card } from 'antd';
import placeholderImage from '../../pictures/aasPlaceholder.png';
import { AasFile } from './AasFile';

function AasOverviewCard({ file, onClick, onContentClick ,pictureSize }) {
  
  const [isHovered, setIsHovered] = useState(false);
  const { Meta } = Card;
  const content = file.content;
  const type = file.type;
  const aasFile = new AasFile(content, type);
  const overview = aasFile.getOverview();
  const image = file.image ? `data:image/png;base64,${file.image}` : placeholderImage;

  const handleImageClick = () => {
    onContentClick(content);
  };

  return (
    <Card
      className='imgCard'
      onClick={onClick}
      hoverable
      style={{ height: 100 + pictureSize }}
      cover={
        <div className='divCard'>
          <img
            alt="example"
            src={image}
            style={{ opacity: isHovered ? 0 : 0.85, width:'100%', objectFit: 'contain', height: 100 + pictureSize }}
            onClick={handleImageClick}
          />
        </div>
      }
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      <Card.Meta
        className='CardMeta'
        description={isHovered && overview ? (
          <>
            <p style={{color: '#D85031', fontSize: `calc(${pictureSize}% - 15%)`}}>ID: {overview.id}</p>
          </>
        ) : null}
      />
      <Meta
        title={<span style={{ fontSize: `calc(${pictureSize}% - 20%)`, color: '#0d3b66' }}>{aasFile.displayName}</span>}
       
        className='meta'
      />
     
    </Card>
  );
}

export default AasOverviewCard