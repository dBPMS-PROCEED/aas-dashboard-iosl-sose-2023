import React, { useState, useEffect } from "react";
import FileUploader from "./FileUploader";
import AasOverviewCard from './AasOverviewCard';
import NavigationTabs from './NavigationTabs';
import TableView from './TableView';
import { Row, Col, Input, Select, Button, Table } from 'antd';
import {
  AppstoreOutlined,
  UnorderedListOutlined,
  PlusOutlined,
  MinusOutlined,
  FormOutlined,
} from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';
//import { SubmodelTemplates } from "./SubmodelTemplates";

import { useAasRegistryStore } from "./AasRegistry";
import { useAasFileStore } from './AasRegistry';


function Dashboard() {
  const navigate = useNavigate();
  const { Option } = Select;

  const [expandedRowKeys, setExpandedRowKeys] = useState([]);

  const [filterText, setFilterText] = useState('');
  const [sortOption, setSortOption] = useState('displayNameAsc');

  const [view, setView] = useState('default');
  const [pictureSize, setPictureSize] = useState(100);
  //const [aasRegistry, setAasRegistry] = useState(new AasRegistry());
  const { aasRegistry,setAasRegistry } = useAasRegistryStore();
  const { aasFileDownload, setAasFileDownload } = useAasFileStore();

  useEffect(() => {
    if(!aasFileDownload) return;
      setAasFileDownload(null);
  }, []);
  const handleFileUpload = (files) => {

    const tempAasRegistry = aasRegistry;
    tempAasRegistry.addFiles(files);
    setAasRegistry(tempAasRegistry);
    files = aasRegistry.getFiles();
    console.log(files);
    //const submodelTemplates = new SubmodelTemplates();
    //console.log(submodelTemplates.templates)
  };
  let files = aasRegistry.getFiles();

  const filteredFiles = files.filter((file) => {
    const displayName = file.displayName || ''; // Use displayName if it's defined, otherwise use null
    return displayName?.toLowerCase().includes(filterText.toLowerCase());
  });



  function safeLocaleCompare(a, b, asc = true) {
    if (a === null || a === undefined) return 1;
    if (b === null || b === undefined) return -1;
    return asc ? a.localeCompare(b) : b.localeCompare(a);
  }
  
  const sortedFiles = filteredFiles.sort((a, b) => {
    switch (sortOption) {
      case 'displayNameAsc':
        return safeLocaleCompare(a.displayName, b.displayName);
      case 'displayNameDesc':
        return safeLocaleCompare(a.displayName, b.displayName, false);
      case 'numberOfSubmodelElementsAsc':
        return a.numberOfSubmodelElements - b.numberOfSubmodelElements;
      case 'numberOfSubmodelElementsDesc':
        return b.numberOfSubmodelElements - a.numberOfSubmodelElements;
      case 'assetKindAsc':
        return safeLocaleCompare(a.assetKind, b.assetKind);
      case 'assetKindDesc':
        return safeLocaleCompare(a.assetKind, b.assetKind, false);
      default:
        return 0;
    }
  });
  


  const handleViewClick = (selectedView) => {
    setView(selectedView);
  };

  const handleIncreaseSize = () => {
    if (pictureSize < 130) {
      setPictureSize(pictureSize + 10);
    }
  };
  
  const handleDecreaseSize = () => {
    if (pictureSize > 90) {
      setPictureSize(pictureSize - 10);
    }
  };


const handleExpand = (expanded, record) => {
  if (expanded) {
    setExpandedRowKeys(prev => [...prev, record.key]);
  } else {
    setExpandedRowKeys(prev => prev.filter(key => key !== record.key));
  }
};


  const handleContentClick = (content) => {
    // Find the specific AasFile from the aasRegistry
    console.log("content",content);
    const aasFile = aasRegistry.getFiles().find(file => file.fileName === content.fileName);
   console.log("dashboardAAsFILE", aasFile);
    console.log("fileID", aasFile?.fileName);
    console.log("contentID",content.name);

    // If the AasFile is found, navigate to MachinePage with the AasFile data
    if (aasFile) {
      const treeData = aasFile.environmentTree;
      const image = aasFile.image;
      console.log("aasFile",treeData);
      navigate('/content-page', { state: { treeData , image, aasFile} });
    }
  };


  const renderContent = () => {
    if (view === 'default') {
      return <TableView files={filteredFiles} handleContentClick={handleContentClick} handleExpand={handleExpand} expandedRowKeys={expandedRowKeys} />;
    } else if (view === 'pictures') {
      return (
        <div>
          <div style={{ marginTop: '5px' }}>
            <Button className="picView" onClick={handleIncreaseSize} icon={<PlusOutlined />} />
            <Button className="listView" onClick={handleDecreaseSize}  icon={<MinusOutlined />} />
          </div>

          <Row justify="space-evenly" className="cardRows">
            {filteredFiles.map((file, index) => (
              <Col 
              sm={pictureSize <= 80
                ? 4
                  :pictureSize <= 100 && pictureSize > 80
                  ? 5
                  : pictureSize >= 110 && pictureSize < 120
                  ? 6
                  : pictureSize >= 120
                  ? 7
                  : 8
                }
              md={
                pictureSize <= 80
                ? 3
                  :pictureSize <= 100 && pictureSize > 80
                  ? 4
                  : pictureSize >= 110 && pictureSize < 120
                  ? 5
                  : pictureSize >= 120
                  ? 6
                  : 7
              }
              lg={
                pictureSize <= 80
                ? 2
                  :pictureSize <= 100 && pictureSize > 80
                  ? 3
                  : pictureSize >= 110 && pictureSize < 120
                  ? 4
                  : pictureSize >= 120
                  ? 5
                  : 6
              }
              
              key={index}
              style={{ paddingLeft: '1%',paddingBottom: '5%' }}
            >
              <AasOverviewCard
                file={file}
                onClick={() => handleContentClick(file)}
                pictureSize={pictureSize}
              />
            </Col>
            
            ))}
          </Row>

        </div>
      );
    }
    console.log("picture Size", pictureSize);

  };

  return (
    <div style={{marginTop:'5px'}}>
      
       <Button
        className="picView"
       type={view === 'pictures' ? 'primary' : 'default'}
       onClick={() => handleViewClick('pictures')}

       icon={<AppstoreOutlined />}
      />
       <Button className="listView"
       type={view === 'default' ? 'primary' : 'default'}
       onClick={() => handleViewClick('default')}
       icon={<UnorderedListOutlined />} />

      <Input.Search className="search"
        placeholder="Filter by display name"
        onSearch={value => {
          console.log(value); // Check the value entered in the search box
          setFilterText(value);
        }}

      />

<Select className="sorting" 
  defaultValue="displayNameAsc" 
  onChange={value => setSortOption(value)} 
>
  <Option value="displayNameAsc" >Sort by Name (A-Z)</Option>
  <Option value="displayNameDesc" >Sort by Name (Z-A)</Option>
  <Option value="numberOfSubmodelElementsAsc" >Sort by no. of Submodels (Asc)</Option>
  <Option value="numberOfSubmodelElementsDesc" >Sort by no. of Submodels (Des)</Option>
  <Option value="assetKindAsc" >Sort by Asset Kind (Asc)</Option>
  <Option value="assetKindDesc" >Sort by Asset Kind (Des)</Option>
</Select>

      <Button className="create-new-machine-btn"  onClick={() => navigate('/newFile')} icon={<FormOutlined /> }>Create a new machine</Button>
      <FileUploader onFileUpload={handleFileUpload} />
      {renderContent()}
    </div>
  );
}

export default Dashboard;
