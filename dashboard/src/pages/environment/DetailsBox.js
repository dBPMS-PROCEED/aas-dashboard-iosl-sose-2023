import { Descriptions, Input } from 'antd';
import { useState } from 'react';
import tv4 from 'tv4';
import schemaFile from "./schema.json";
import { useAasFileStore } from './AasRegistry';
import Ajv from "ajv/dist/2019";

function DetailsBox({ details, treeData, aasFile, onValueChange }) {
  const [modifiedDetails, setModifiedDetails] = useState({});
  const [fieldStatus, setFieldStatus] = useState({}); // State to track modified fields
  const [fieldValidity, setFieldValidity] = useState({}); // State to track field validity
  const { aasFileDownload, setAasFileDownload } = useAasFileStore(); // Use AasFileStore

  const updateFieldStatus = (key, isModified = true) => {
    setFieldStatus((prevStatus) => {
      const updatedStatus = { ...prevStatus, [key]: isModified };
      console.log('updated fieldStatus', updatedStatus);
      return updatedStatus;
    });
  };

  const updateFieldValidity = (key, isValid = true) => {
    setFieldValidity((prevValidity) => {
      const updatedValidity = { ...prevValidity, [key]: isValid };
      console.log('updated fieldValidity', updatedValidity);
      return updatedValidity;
    });
  };


  const handleValueChange = (key, value, oldValue) => {
    const updatedDetails = { ...details, [key]: value };
    setModifiedDetails(prevDetails => ({ ...prevDetails, ...updatedDetails }));
    onValueChange(updatedDetails);
    //updateFieldStatus(key);
    //console.log("---------------------");
    //console.log(key);
    //console.log(value);
    //console.log(oldValue);
    //console.log("---------------------");
    //console.log(aasFile.content)
    // Here i try to validate the file we already have
    const fileToCheck2 = JSON.parse(aasFile.content);
    const ajv = new Ajv(); // Create an instance of Ajv
    const validate = ajv.compile(schemaFile); // Compile the JSON schema
    const isValid3 = validate(fileToCheck2); // Validate the fileToCheck against the schema
    const isValid2 = tv4.validate(fileToCheck2, schemaFile);
    console.log("isvalid2Field",isValid3);

    // Update the JSON file
    const updatedJsonObject = updateJSONKeys(JSON.parse(aasFile.content), key, value, oldValue);
    console.log(JSON.stringify(updatedJsonObject));
    aasFile.content = JSON.stringify(updatedJsonObject);
    if(aasFile.image){
      setAasFileDownload({ ...aasFileDownload, content: aasFile.content, image: aasFile.image}); // Update AasFileStore
    }
    else setAasFileDownload({ ...aasFileDownload, content: aasFile.content}); // Update AasFileStore

    // Validate the file against the schema
    const fileToCheck = JSON.parse(aasFile.content);
    console.log("fileToCheck", fileToCheck);
    const isValid = validate(fileToCheck, schemaFile);

    //updateFieldValidity(key, isValid);
    console.log('fieldStatus before update', fieldStatus);
console.log('fieldValidity before update', fieldValidity);
updateFieldStatus(key);
updateFieldValidity(key, isValid);
console.log('fieldStatus after update', fieldStatus);
console.log('fieldValidity after update', fieldValidity);

  };

  const updateJSONKeys = (obj, key, value, oldValue) => {
    if (typeof obj !== 'object') {
      return obj;
    }

    if (Array.isArray(obj)) {
      return obj.map((item) => updateJSONKeys(item, key, value, oldValue));
    }

    return Object.keys(obj).reduce((result, objKey) => {
      let updatedValue;
      if (objKey === key && obj[objKey] === oldValue) {
        updatedValue = value;
      } else {
        updatedValue = updateJSONKeys(obj[objKey], key, value, oldValue);
      }
      return { ...result, [objKey]: updatedValue };
    }, {});
  }







  return (
    <Descriptions column={1} className='box'>
      {details &&
        Object.entries(details).map(([key, value]) => (
          <Descriptions.Item key={key} label={key}>
            <Input
              value={modifiedDetails[key] || value}
              onChange={(e) => handleValueChange(key, e.target.value, value)}
              style={{
                borderColor: fieldStatus[key]
                  ? fieldValidity[key]
                    ? 'green'
                    : 'red'
                  : undefined,
              }}
            />
          </Descriptions.Item>
        ))}
    </Descriptions>
  );
}

export default DetailsBox;
