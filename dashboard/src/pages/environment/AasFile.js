import { transformJsonToTreeData } from './TransformJsonToTree';
export class AasFile {
  constructor(content, type, lastModified,image,fileName) {
    // key is necessary for displaying the data in the default view
    this.key = null;
    this.content = content;
    this.type = type;
    this.environment = null;
    this.environmentTree = null;
    this.error = null;
    this.lastModified = lastModified;
    this.fileName = fileName;

    this.overview = null;
    this.displayName = "none";
    this.assetKind = null;
    this.image = image;
    this.submodelElements = [];
    this.submodels = null;
    this.numberOfSubmodelElements = 0;
    this.barInformation = null;
    this.parse();
  }

  parse() {
    try {
      if (this.type === "application/json") {
        const jsonable = JSON.parse(this.content);
        console.log("Parsed JSON", jsonable);
        if (jsonable) {
          this.environment = jsonable;
          this.environmentTree = transformJsonToTreeData(jsonable);

          this.displayName = this.environment?.assetAdministrationShells?.[0]?.displayName?.[0]?.text;
          this.overview = {
            idShort: jsonable?.assetAdministrationShells?.[0]?.idShort,
            id: jsonable?.assetAdministrationShells?.[0]?.id,
            assetInformation: jsonable?.assetAdministrationShells?.[0]?.assetInformation
          };
    
          this.assetKind = this.environment?.assetAdministrationShells?.[0]?.assetInformation?.assetKind;
          this.submodels = this.environment?.submodels;
          this.numberOfSubmodelElements = this.submodels.length;
        
          this.submodelElements = this.submodels?.submodelElements;
          console.log(this.submodels);
        }
      } else if (this.type === "text/xml") {
        const xml2js = require('xml2js');

        const xml = this.content;  // your XML string here
        let jsonable = null;
        xml2js.parseString(xml, { explicitArray: false, mergeAttrs: true }, (err, result) => {
          if (err) {
            console.error(err);
          } else {
            jsonable = JSON.stringify(result, null, 2);
          }
        });
        console.log(jsonable);
        jsonable = JSON.parse(jsonable);
        console.log("here", jsonable.environment.assetAdministrationShells.assetAdministrationShell);
        if (jsonable) {
            console.log("test");
          this.environment = jsonable.environment.assetAdministrationShells.assetAdministrationShell;
          this.environmentTree = transformJsonToTreeData(jsonable);
          console.log("env", this.environmentTree);
            // Structure the data for a better overview when displaying machines
            if(this.environment.displayName.langStringNameType.text != null){
                this.displayName = this.environment.displayName.langStringNameType.text;
            }
            this.overview = {
                idShort: this.environment.idShort,
                id: this.environment.id,
                assetInformation: this.environment.assetInformation
              };
            this.assetKind = this.environment.assetInformation.assetKind;
              console.log(this.assetKind);
        }
      }
    } catch (error) {
      this.error = error;
    }
  }

  updateEnvironmenTree(){
    const jsonable = JSON.parse(this.content);
    if (jsonable) {
      this.environmentTree = transformJsonToTreeData(jsonable);
    }
  }
  getEnvironmentTree(){
    return this.environmentTree;
  }
  getOverview(){
    return this.overview;
  }
  getGraphInformation(){
    const assetKind = this.environment.assetInformation.assetKind;
  }
}





