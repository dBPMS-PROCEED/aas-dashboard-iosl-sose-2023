import { useState, useEffect } from "react";
import { Tree } from "antd";
import DetailsBox from "./DetailsBox";
import "../../styles/index.css";
import { useAasFileStore } from "./AasRegistry";

function MyTree({ treeData, aasFile }) {
  const [details, setDetails] = useState({});
  const [selectedNodeKey, setSelectedNodeKey] = useState(null);
  const [expandedKeys, setExpandedKeys] = useState([]);
  const { aasFileDownload, setAasFileDownload } = useAasFileStore();

  const isNodeSelected = selectedNodeKey !== null;
  // Handler for updating details when they change
  function handleValueChange(modifiedDetails) {
    setDetails(modifiedDetails);
  }
  // On mount, set the aasFileDownload in the Zustand store if it's not already set
  useEffect(() => {
    if (aasFileDownload) return;
    setAasFileDownload(aasFile);
  }, []);
  // Handler for when a node in the tree is selected
  function onSelect(selectedKeys, info) {
    if (info.selectedNodes.length > 0) {
      const selectedNode = info.selectedNodes[0];
      setDetails(selectedNode.leaves);
      setSelectedNodeKey(selectedKeys[0]);
    } else {
      setSelectedNodeKey(null);
    }
  }
  // Handler for when a node in the tree is expanded
  function onExpand(expandedKeys) {
    setExpandedKeys(expandedKeys);
    // If the currently selected node is no longer in the expanded keys list, deselect it.
    if (selectedNodeKey && !expandedKeys.includes(selectedNodeKey)) {
      setSelectedNodeKey(null);
    }
  }

  return (
    <div className="MyTree-div">
      <div className="blankSpace1" />
      <div>
        <Tree
          style={{ backgroundColor: "#CEE1FA" }}
          treeData={treeData}
          onSelect={onSelect}
          onExpand={onExpand}
          expandedKeys={expandedKeys}
        />
      </div>
      <div className="blankSpace2" />
      {isNodeSelected && (
        <div className="detailsBox">
          <DetailsBox
            details={details}
            treeData={treeData}
            aasFile={aasFile}
            onValueChange={handleValueChange}
          />
        </div>
      )}
    </div>
  );
}

export default MyTree;
