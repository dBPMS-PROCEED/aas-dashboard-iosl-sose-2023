import React, { useState, useEffect } from 'react';
import { Row, Col, Button, Typography, Card } from 'antd';
import { useAasRegistryStore } from '../environment/AasRegistry';
import NavigationTabs from '../environment/NavigationTabs';
import { chartValues } from './ChartValues';
import { LineChart, Line, BarChart, Bar, PieChart, Pie, XAxis, YAxis, CartesianGrid, Cell, Tooltip, Legend } from 'recharts';

const { Text } = Typography;
const { Meta } = Card;

const ChartsPage = () => {
  const { aasRegistry,setAasRegistry } = useAasRegistryStore();
  // declare a new state variable
  const [calculatedChartValues, setCalculatedChartValues] = useState(null);

  useEffect(() => {
    // Now inside this useEffect, you can update calculatedChartValues
    const chartVals = chartValues(aasRegistry.aasFiles);
    setCalculatedChartValues(chartVals);  // update the state
    console.log("uploaded Files", chartVals.assetTypeChart); 
  }, []);

  const data = [
    { year: '2015', sales: 800 },
    { year: '2016', sales: 1200 },
    { year: '2017', sales: 1500 },
    { year: '2018', sales: 2000 },
    { year: '2019', sales: 1800 },
    { year: '2020', sales: 2100 },
  ];
  const colors = [ '#1984c5', '#48b5c4', '#a6d75b', '#c9e52f', '#d0ee11'];

  console.log("aa",calculatedChartValues ? calculatedChartValues.machineTypeChart : []);

  
  const renderCharts = () => {
    const chartComponents = [
      {
        title: 'Available machine types',
        component: (
          <BarChart 
            width={350} 
            height={350} 
            data={calculatedChartValues ? calculatedChartValues.machineTypeChart : []}
          >
            <XAxis   dataKey="name" 
            
  angle={-45} 
  textAnchor="end" 
  interval={0} 
  height={100} label={{ value: '', position: 'insideBottom', dy: 10 }} />
            <YAxis 
              label={{ value: '', angle: -90, position: 'insideLeft', dy: -10 }}
              ticks={Array.from({ length: Math.max(...(calculatedChartValues ? calculatedChartValues.machineTypeChart.map(item => item.value) : [])) + 1 }, (_, i) => i)}
            />
            <CartesianGrid strokeDasharray="3 3" />
            <Tooltip />
            <Bar dataKey="value">
              {data.map((entry, index) => (
                <Cell key={`cell-${index}`} fill={colors[index % colors.length]} />
              ))}
            </Bar>
          </BarChart>
        ),
      },
      
      {
        title: 'Common submodel elements',
        component: (
          <BarChart 
            width={350} 
            height={370} 
            data={calculatedChartValues ? calculatedChartValues.submodelElementChart : []}
          >
            <XAxis 
              angle={-45} 
              textAnchor="end" 
              interval={0} 
              height={120}
            dataKey="name" label={{ value: 'Submodel Element', position: 'insideBottom', dy: 20 }} />
            <YAxis 
              label={{ value: '', angle: -90, position: 'insideLeft', dy: -10 }}
              ticks={Array.from({ length: Math.max(...(calculatedChartValues ? calculatedChartValues.submodelElementChart.map(item => item.value) : [])) + 1 }, (_, i) => i)}
            />
            <CartesianGrid strokeDasharray="3 3" />
            <Tooltip />
            <Bar dataKey="value">
              {data.map((entry, index) => (
                <Cell key={`cell-${index}`} fill={colors[index % colors.length]} />
              ))}
            </Bar>
          </BarChart>
        ),
      },
           
      
      {
        title: 'Available asset Types',
        component: (
          <PieChart width={350} height={300}>
            <Pie data={calculatedChartValues ? calculatedChartValues.assetTypeChart : []} dataKey="value" nameKey="name" cx="50%" cy="50%" outerRadius={100}>
              {calculatedChartValues ? calculatedChartValues.assetTypeChart.map((entry, index) => (
                <Cell key={`cell-${index}`} fill={colors[index % colors.length]} />
              )) : null}
            </Pie>
            <Tooltip />
            <Legend />
          </PieChart>
        ),
      }
      
    ];

    const rows = [];
    let currentRow = [];
    chartComponents.forEach((chart, index) => {
      currentRow.push(
        <Col sm={{span:24, offset: 4}} md={{span:12, offset: 6}} lg={{span:8, offset:0}} key={index} className='chartsCol'>
          <Text className='chartsTitle'>
            {chart.title}
          </Text>
          {chart.component}
        </Col>
      );
      if (currentRow.length === 3 || index === chartComponents.length - 1) {
        rows.push(
          <Row gutter={16} key={index}>
            {currentRow}
          </Row>
        );
        currentRow = [];
      }
    });

    return rows;
  };

  let totalMachines = 0;
  if (calculatedChartValues && calculatedChartValues.machineTypeChart) {
    totalMachines = calculatedChartValues.machineTypeChart.reduce(
      (sum, item) => sum + item.value,
      0
    );
  }
  return (
    <div>
      
      <div className="available-machines">
    <div className="title">Number of available machines</div>
    <div className="count">{totalMachines}</div>
  </div>
        
      
    <div style={{  marginLeft: '5%', marginBottom: '100px' }}>
      {renderCharts()}
    </div>
    </div>
  );
};

export default ChartsPage;
