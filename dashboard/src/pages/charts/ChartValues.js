const levenshtein = require('fast-levenshtein');
export function chartValues(data){
  let assetTypeChart = null;
  let machineTypeChart = null;
  let submodelElementChart = null;
  
  console.log("data", data);
  const idShorts = data.map(aasFile => aasFile.overview.id);
  console.log("idShorts",idShorts);
  const types = data.map(aasFile => aasFile.assetKind);
  console.log("types", types);
  console.log(matchesGroup("WindTurbine-001", "windturbine")); // Outputs: true
  console.log(matchesGroup("WindTurbin-001", "windturbine")); // Outputs: true
  console.log(matchesGroup("SolarPanel-001", "windturbine")); // Outputs: false


  assetTypeChart = countAssetKinds(types);
  console.log("counts",assetTypeChart);
  
  machineTypeChart = groupMachinesByType(idShorts);
  console.log("chartData", machineTypeChart);

  const submodels = data.map(aasFile => aasFile.submodels);
  console.log("submodels", submodels);

  submodelElementChart = countSubmodels(submodels);
console.log("submodelData", submodelElementChart);

return {
  assetTypeChart,
  machineTypeChart,
  submodelElementChart
}
  // Use chartData for the Ant Design charts
  
}

function countSubmodels(submodels) {
  const counts = {};

  submodels.forEach(submodelArray => {
    if (submodelArray) {
      submodelArray.forEach(model => {
        const modelIdShort = model.idShort;

        if (!counts[modelIdShort]) {
          // initialize the count for this model
          counts[modelIdShort] = 0;
        }

        // increment the count for this model
        counts[modelIdShort]++;
      });
    }
  });

  // Sort submodels by their counts and select top 5
  const sortedSubmodels = Object.keys(counts).sort((a, b) => counts[b] - counts[a]).slice(0, 5);

  // Convert the counts object to an array of objects for the charts
  const chartData = sortedSubmodels.map(submodelName => ({
    name: submodelName,
    value: counts[submodelName],
  }));

  return chartData;
}


function countAssetKinds(assetKinds) {
  let counts = {
    "Instance": 0,
    "Type": 0,
    "NotApplicable": 0
  };

  assetKinds.forEach(kind => {
    if (kind === "Instance" || kind === "Type") {
      counts[kind]++;
    } else {
      counts["NotApplicable"]++;
    }
  });

  const result = Object.keys(counts).map(label => ({
    name: label,
    value: counts[label],
  }));

  return result;
}


function groupMachinesByType(machines) {
  let groups = {
    "windturbine": 0,
    "solarpanel": 0,
    "temperatureSensor":0,
    "pressuresensor":0,
    "undefined": 0
  };

  machines.forEach(machine => {
    const idShort = machine;
    const groupLabels = Object.keys(groups);

    let matchedGroup = false;

    groupLabels.forEach(groupLabel => {
      if (groupLabel !== "undefined" && matchesGroup(idShort, groupLabel)) {
        groups[groupLabel]++;
        matchedGroup = true;
      }
    });

    if (!matchedGroup) {
      groups["undefined"]++;
    }
  });

  const chartData = Object.keys(groups).map(groupLabel => ({
    name: groupLabel,
    value: groups[groupLabel],
  }));

  return chartData;
}




  function matchesGroup(idShort, groupLabel, similarityThreshold = 0.85) {
    // Convert both strings to lower case for a case-insensitive comparison
    const lowerCaseIdShort = idShort.toLowerCase();
    const lowerCaseGroupLabel = groupLabel.toLowerCase();
  
    // Calculate the maximum allowed distance for the desired similarity
    const maxDistance = (1 - similarityThreshold) * lowerCaseGroupLabel.length;
  
    // Slide the window over the idShort
    for (let i = 0; i <= lowerCaseIdShort.length - lowerCaseGroupLabel.length; i++) {
      // Extract the substring of the idShort that's inside the window
      const substring = lowerCaseIdShort.substring(i, i + lowerCaseGroupLabel.length);
  
      // Calculate the Levenshtein distance between the substring and group label
      const distance = levenshtein.get(substring, lowerCaseGroupLabel);
  
      
  
      // Check if the distance is less than or equal to the maximum allowed distance
      if (distance <= maxDistance) {
        return true;
      }
    }
  
    // If no matching substring was found, return false
    return false;
  }
  
  
  
  
  