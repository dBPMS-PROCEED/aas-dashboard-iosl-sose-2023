import React from 'react';
import { Layout, Col } from 'antd';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Dashboard from './pages/environment/dashboard';
import MachinePage from './pages/environment/MachinePage';
import NewFile from './pages/environment/NewFile';
import Charts from './pages/charts/charts'
import NavigationTabs from './pages/environment/NavigationTabs';

const { Footer: AntFooter } = Layout; 
const { Content } = Layout;

function App() {
  return (
    <Router> 
      <Layout className='layout'>

        <NavigationTabs />
        <Content className='content'>
          <Routes>
            <Route path="/" element={<Dashboard />} />
            <Route path="/content-page" element={<MachinePage />} />
            <Route path= '/newFile' element={<NewFile />} />
            <Route path = '/charts' element={<Charts />} />
          </Routes>
        </Content>
       
        <footer style={{height: '5px'}}>
          <AntFooter className="antFooter">
           <Col span={12}  offset={6}>
              © 2023, AAS Dashboard 
           </Col>
          </AntFooter>
        </footer>
          
       
      </Layout>
    </Router>
  );
};

export default App;
